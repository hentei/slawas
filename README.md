Weakauras for World of Warcraft Classic live servers.

The latest version of WeakAuras is needed.

To install, just copy paste the string into the import field of WA.

Always delete the whole old WA first and import the newer version after.

Works on english client, other languages might be working too, just try it :)

Cheers

 
| **IMPORTANT FOR DRUID WA** | 
| ------ | 
| In order to work as intended you should learn Aquatic Form before learning Cat Form.<br>Otherwise you need to change the StanceTrigger for all Cat WAs. :) |
